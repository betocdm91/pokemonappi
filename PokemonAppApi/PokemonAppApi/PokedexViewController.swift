//
//  PokedexViewController.swift
//  PokemonAppApi
//
//  Created by Carlos Osorio on 20/6/18.
//  Copyright © 2018 Carlos Osorio. All rights reserved.
//

import UIKit

class PokedexViewController: UIViewController {

    @IBOutlet weak var pokeimage: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var pesoLabel: UILabel!
    @IBOutlet weak var alturaLabel: UILabel!
    var pokemon:[Pokemon] = []

    
    override func viewDidLoad() {
        super.viewDidLoad()

        let network = Network()
        network.getAllPokemon{
            (pokemonArray) in
            self.pokemon = pokemonArray
            
            
            self.nameLabel.text = self.pokemon[0].name
            //self.pesoLabel.text = self.pokemon[0].weight
            //self.alturaLabel.text = self.pokemon[0].height
            
        }
        
        let net = Network()
        net.getImage(<#Int#>){
            (id) in
            self.pokeimage.image
        }
        
    }

}
