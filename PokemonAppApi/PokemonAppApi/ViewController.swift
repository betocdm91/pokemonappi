//
//  ViewController.swift
//  PokemonAppApi
//
//  Created by Carlos Osorio on 13/6/18.
//  Copyright © 2018 Carlos Osorio. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var pokemonTable: UITableView!
    
    var pokemon:[Pokemon] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let network = Network()
        network.getAllPokemon { (pokemonArray) in
            self.pokemon = pokemonArray
            self.pokemonTable.reloadData()
        }
        // Do any additional setup after loading the view, typically from a nib.
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return pokemon.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        cell.textLabel?.text = pokemon[indexPath.row].name
        
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.section == 0{
            performSegue(withIdentifier: "pokeconnect", sender: self)
            
        }
        
    }

}

